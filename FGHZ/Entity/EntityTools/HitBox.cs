﻿namespace FGHZ.Entity.EntityTools
{
    class HitBox
    {
        private readonly int nSizeY, nSizeX;
        private readonly float fMiddleY, fMiddleX;

        public int SizeY { get => nSizeY; }
        public int SizeX { get => nSizeX; }
        public double MiddleY { get => fMiddleY; }
        public double MiddleX { get => fMiddleX; }

        public HitBox(int nSizeY, int nSizeX)
        {
            this.nSizeY = nSizeY;
            this.nSizeX = nSizeX;

            fMiddleY = nSizeY / 2;
            fMiddleX = nSizeX / 2;
        }

        public HitBox(HitBox hitbox)
        {
            nSizeX = hitbox.nSizeX;
            nSizeY = hitbox.nSizeY;

            fMiddleX = hitbox.fMiddleX;
            fMiddleY = hitbox.fMiddleY;
        }

        public bool DoCollide(HitBox comp, Position pos, Position compPos)
        // pos : pos of calling object; compPos: pos of comparition object; comp: comparision hitbox
        {
            if (pos.PosX + nSizeX - 1 < compPos.PosX)
                return false;
            if (pos.PosX > compPos.PosX + comp.nSizeX - 1)
                return false;
            if (pos.PosY + nSizeY - 1 < compPos.PosY)
                return false;
            if (pos.PosY > compPos.PosY + comp.nSizeY - 1)
                return false;

            return true;
        }
    }
}
