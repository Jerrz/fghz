﻿using FGHZ.Entity.Objects;
using FGHZ.Entity.Units.Enemies;
using FGHZ.Entity.Units.Player;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace FGHZ.Engine
{
    class LevelFileHandler
    {
        private string path;
        private int nHeight, nWidth;

        public LevelFileHandler(string path, int nHeight, int nWidth)
        {
            this.path = path;
            this.nHeight = nHeight;
            this.nWidth = nWidth;
        }

        public LevelData ReadSingleFile(string file)
        {
            LevelData lvl = new LevelData(nHeight, nWidth);
            string filePath = file;
            Console.WriteLine(filePath);

            using (StreamReader r = new StreamReader(filePath))
            {
                string json = r.ReadToEnd();
                dynamic contents = JsonConvert.DeserializeObject(json);

                lvl.player = new Player((int)contents.player.yPos, (int)contents.player.xPos);

                foreach (dynamic e in contents.enemies)
                {
                    switch ((string)e.type)
                    {
                        case ("enemy_normal"):
                            lvl.enemies.Add(new Enemy_normal((int)e.yPos, (int)e.xPos));
                            break;
                        case ("enemy_strong"):
                            lvl.enemies.Add(new Enemy_strong((int)e.yPos, (int)e.xPos));
                            break;
                        case ("enemy_tank"):
                            lvl.enemies.Add(new Enemy_tank((int)e.yPos, (int)e.xPos));
                            break;
                        case ("enemy_thief"):
                            lvl.enemies.Add(new Enemy_thief((int)e.yPos, (int)e.xPos));
                            break;
                        default:
                            throw new Exception("Unknown Enemy-Type");
                    }
                }

                foreach (dynamic e in contents.destructables)
                {
                    switch ((string)e.type)
                    {
                        case ("barrel"):
                            lvl.destructables.Add(new Barrel((int)e.yPos, (int)e.xPos));
                            break;
                        case ("exploding_barrel"):
                            lvl.destructables.Add(new ExplosiveBarrel((int)e.yPos, (int)e.xPos));
                            break;
                        default:
                            throw new Exception("Unknown Destructable-Type");
                    }
                }

                foreach (dynamic e in contents.indestructables)
                {
                    switch ((string)e.type)
                    {
                        case ("chest"):
                            lvl.indestructables.Add(new Chest((int)e.yPos, (int)e.xPos));
                            break;
                        case ("wall"):
                            lvl.indestructables.Add(new Wall((int)e.yPos, (int)e.xPos, (int)e.ySize, (int)e.xSize));
                            break;
                        default:
                            throw new Exception("Unknown Indestructable-Type");
                    }
                }

            }

            return lvl;
        }

        public List<LevelData> ReadFolder()
        {
            List<LevelData> levels = new List<LevelData>();
            string[] files = Directory.GetFiles(path);
            foreach (string file in files)
            {
                levels.Add(ReadSingleFile(file));
            }
            return levels;
        }
    }
}
