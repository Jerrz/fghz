﻿using FGHZ.Engine;
using FGHZ.Entity.EntityTools;
using System;
using System.Collections.Generic;
using System.Text;

namespace FGHZ.Entity
{
    abstract class DestructableEntity : BaseEntity
    {
        protected float fHP;
        private float maxHP;

        public DestructableEntity(int nPosY, int nPosX, int nSizeY, int nSizeX, float maxHP) : base(nPosY, nPosX, nSizeY, nSizeX)
        {
            fHP = maxHP;
            this.maxHP = maxHP;

        }

        // Constructor for random placement
        public DestructableEntity(int nSizeY, int nSizeX, float maxHP, LevelData data) : base(nSizeY, nSizeX, data)
        {
            fHP = maxHP;
            this.maxHP = maxHP;

        }

        public void IncreaseHealth(float fInc)
        {
            if (fHP + fInc > maxHP)
            {
                fHP = maxHP;
            }
            else
            {
                fHP += fInc;
            }
        }

        public void DecreaseHealth(float fDec)
        {
            fHP -= fDec;
        }

        public bool IsDestroyed()
        {
            return fHP <= 0;
        }
    }
}
