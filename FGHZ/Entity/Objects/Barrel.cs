﻿using System;
using FGHZ.Engine;

namespace FGHZ.Entity.Objects
{
    class Barrel : DestructableObject
    {
        readonly Potion loot;
        static float barrelStartHP = 1;
        static readonly Tile upperLeft = new Tile('O', ConsoleColor.Green);
        static Random rng = new Random();       //for creating loot
        bool filled;

        //Barrel-Size is always 2x2
        public Barrel(int nPosY, int nPosX) : base(nPosY, nPosX, 2, 2, barrelStartHP) 
        {
            this.loot = GenerateLoot();
            this.filled = true;
        }

        //Barrels give Potions (for now)
        private Potion GenerateLoot()
        {
            int worth = rng.Next(20,81);
            Potion p = new Potion(worth);
            return p;
        }

        public override void Draw(Tilemap map)
        {
            int y = pos.PosY;
            int x = pos.PosX;
            map.SetTile(y, x, upperLeft);
            map.SetTile(y+1, x, upperLeft);
            map.SetTile(y, x+1, upperLeft);
            map.SetTile(y+1, x+1, upperLeft);
        }

        public override void Update(LevelData data, ConsoleKey? keyPressed)
        {
            if (filled)
            {
                if (this.fHP <= 0)
                {
                    data.player.AddPotion(loot.Healing);
                    filled = false;
                }
            }
        }
    }
}
