﻿using FGHZ.Entity.EntityTools;
using FGHZ.Entity.Units.Player;
using FGHZ.Entity.Units;
using System;
using System.Collections.Generic;
using System.Text;

namespace FGHZ.Entity
{
    abstract class IndestructableEntity : BaseEntity
    {
        public IndestructableEntity(int nPosY, int nPosX, int nSizeY, int nSizeX) : base(nPosY, nPosX, nSizeY, nSizeX) { }

        public abstract bool Interact(Unit unit);
    }
}
