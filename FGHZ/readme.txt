#######################################
#  FGHZ - A Bird-Based Hack' n Slash  #
#######################################

Movement:
---------
W, A, S, D or Arrow Keys to move
Spacebar to attack
E to interact with objects ( e.g. chests )
1-9 to use the potion in the corresponding inventory space

Game mechanics:
---------------
Chests:
To improve your equipment ( current is visible on the bottom of the screen ) collect new equipment from chests

Barrels:
Destroy barrels to gain potions to heal yourself ( your current potions are on the right of the screen )
Beware of exploding barrels!

Gamemodes:
----------
Normal:
Complete each of the three included levels by eliminating all enemy-birds

Endless:
Fight continously-spawning enemies as long as possible

Custom:
Create your own custom levels in ./Levels/custom ( example-Level 'Custom1.json' already includes every type of entity and the required structure )
Your custom levels will be played one after another ( like the normal gammode ) - alphabetically sorted