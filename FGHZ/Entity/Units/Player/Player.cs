﻿using FGHZ.Engine;
using FGHZ.Entity.EntityTools;
using FGHZ.Entity.Objects;
using FGHZ.Entity.Units.Enemies;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection.Metadata;
using System.Text;
using System.Threading;

namespace FGHZ.Entity.Units.Player
{
    class Player : Unit

    {
        List<Tile> charTiles = new List<Tile>();
        Tile[] invHUD = new Tile[9];
        Tile[] statsHUD = new Tile[29];
        Potion[] potions = new Potion[9];

        static readonly Tile rBeak = new Tile('>', System.ConsoleColor.Black);
        static readonly Tile lBeak = new Tile('<', System.ConsoleColor.Black);
        static readonly Tile eyes = new Tile('"', System.ConsoleColor.Black);
        static readonly Tile rHead = new Tile('(', System.ConsoleColor.Black);
        static readonly Tile lHead = new Tile(')', System.ConsoleColor.Black);
        static readonly Tile rStomach = new Tile(')', System.ConsoleColor.Black);
        static readonly Tile lStomach = new Tile('(', System.ConsoleColor.Black);
        static readonly Tile rBack = new Tile('\u002F', System.ConsoleColor.Black);
        static readonly Tile lBack = new Tile('\u005C', System.ConsoleColor.Black);
        static readonly Tile lWeapon = new Tile('\u005C', System.ConsoleColor.Black);
        static readonly Tile rWeapon = new Tile('\u002F', System.ConsoleColor.Black);

        bool lookingLeft = true;
        static readonly int Y_SIZE = 2;
        static readonly int X_SIZE = 3;
        static readonly int HP = 30;
        static readonly Equipment start = new Equipment(0, 5, 5);

        public Player(int nPosY, int nPosX) : base(nPosY, nPosX, Y_SIZE, X_SIZE, HP, start)
        {
            AddPotion(5);
            AddPotion(15);
        }

        public void AddPotion(float healing)
        {
            for (int i = 0; i < 8; i++)
            {
                if (potions[i] == null)
                {
                    potions[i] = new Potion(healing);
                    return;
                }
            }
        }

        public Tile[] GetInvHUD()
        {
            for (int i = 0; i < 9; i++)
            {
                if (potions[i]!=null)
                    invHUD[i] = new Tile('\u2588', potions[i].Color);
                else
                    invHUD[i] = new Tile('\u2588', System.ConsoleColor.Red);
            }
            return invHUD;
        }

        public Tile[] GetStatsHUD()
        {
            //converting each digit to char.
            double currentHP = (double)fHP;
            char firstDigitHP = char.Parse(Math.Abs(Math.Floor(currentHP / 100)).ToString());
            char secondDigitHP = char.Parse(Math.Abs(Math.Floor((currentHP / 10) % 10)).ToString());
            char thirdDigitHP = char.Parse(Math.Abs((currentHP % 10 % 10)).ToString());

            double currentATK = (double)GetAttack();
            char firstDigitATK = char.Parse(Math.Abs(Math.Floor(currentATK / 100)).ToString());
            char secondDigitATK = char.Parse(Math.Abs(Math.Floor((currentATK / 10) % 10)).ToString());
            char thirdDigitATK = char.Parse(Math.Abs((currentATK % 10 % 10)).ToString());

            double currentDEF = (double)GetArmor();
            char firstDigitDEF = char.Parse(Math.Abs(Math.Floor(currentDEF / 100)).ToString());
            char secondDigitDEF = char.Parse(Math.Abs(Math.Floor((currentDEF / 10) % 10)).ToString());
            char thirdDigitDEF = char.Parse(Math.Abs((currentDEF % 10 % 10)).ToString());

            double currentRA = Math.Round((double)GetRange());
            char firstDigitRA = char.Parse(Math.Abs(Math.Floor(currentRA / 100)).ToString());
            char secondDigitRA = char.Parse(Math.Abs(Math.Floor((currentRA / 10) % 10)).ToString());
            char thirdDigitRA = char.Parse(Math.Abs((currentRA % 10 % 10)).ToString());

            statsHUD[0] = new Tile('H', System.ConsoleColor.Black);
            statsHUD[1] = new Tile('P', System.ConsoleColor.Black);
            statsHUD[2] = new Tile(':', System.ConsoleColor.Black);
            statsHUD[3] = new Tile(firstDigitHP, System.ConsoleColor.Black);
            statsHUD[4] = new Tile(secondDigitHP, System.ConsoleColor.Black);
            statsHUD[5] = new Tile(thirdDigitHP, System.ConsoleColor.Black);
            statsHUD[6] = new Tile('|', System.ConsoleColor.Black); 
            statsHUD[7] = new Tile('A', System.ConsoleColor.Black); 
            statsHUD[8] = new Tile('T', System.ConsoleColor.Black); 
            statsHUD[9] = new Tile('K', System.ConsoleColor.Black); 
            statsHUD[10] = new Tile(':', System.ConsoleColor.Black); 
            statsHUD[11] = new Tile(firstDigitATK, System.ConsoleColor.Black);
            statsHUD[12] = new Tile(secondDigitATK, System.ConsoleColor.Black);
            statsHUD[13] = new Tile(thirdDigitATK, System.ConsoleColor.Black);
            statsHUD[14] = new Tile('|', System.ConsoleColor.Black); 
            statsHUD[15] = new Tile('D', System.ConsoleColor.Black); 
            statsHUD[16] = new Tile('E', System.ConsoleColor.Black); 
            statsHUD[17] = new Tile('F', System.ConsoleColor.Black); 
            statsHUD[18] = new Tile(':', System.ConsoleColor.Black); 
            statsHUD[19] = new Tile(firstDigitDEF, System.ConsoleColor.Black);
            statsHUD[20] = new Tile(secondDigitDEF, System.ConsoleColor.Black);
            statsHUD[21] = new Tile(thirdDigitDEF, System.ConsoleColor.Black);
            statsHUD[22] = new Tile('|', System.ConsoleColor.Black); 
            statsHUD[23] = new Tile('R', System.ConsoleColor.Black); 
            statsHUD[24] = new Tile('A', System.ConsoleColor.Black); 
            statsHUD[25] = new Tile(':', System.ConsoleColor.Black); 
            statsHUD[26] = new Tile(firstDigitRA, System.ConsoleColor.Black);
            statsHUD[27] = new Tile(secondDigitRA, System.ConsoleColor.Black);
            statsHUD[28] = new Tile(thirdDigitRA, System.ConsoleColor.Black);

            return statsHUD;
        }


        public override void Draw(Tilemap map)
        {
            if (lookingLeft)
            {
                map.SetTile(pos.PosY, pos.PosX, lBeak);
                map.SetTile(pos.PosY, pos.PosX + 1, eyes);
                map.SetTile(pos.PosY, pos.PosX + 2, lHead);
                map.SetTile(pos.PosY + 1, pos.PosX, lWeapon);
                map.SetTile(pos.PosY + 1, pos.PosX + 1, lStomach);
                map.SetTile(pos.PosY + 1, pos.PosX + 2, lBack);
            }
            else
            {
                map.SetTile(pos.PosY, pos.PosX, rHead);
                map.SetTile(pos.PosY, pos.PosX + 1, eyes);
                map.SetTile(pos.PosY, pos.PosX + 2, rBeak);
                map.SetTile(pos.PosY + 1, pos.PosX, rBack);
                map.SetTile(pos.PosY + 1, pos.PosX + 1, rStomach);
                map.SetTile(pos.PosY + 1, pos.PosX + 2, rWeapon);
            }
        }

        public override void Update(LevelData data, ConsoleKey? keyPressed)
        {
            switch(keyPressed)
            {
                case ConsoleKey.S:
                case ConsoleKey.DownArrow:
                    Move(1, 0, data);
                    break;
                case ConsoleKey.W:
                case ConsoleKey.UpArrow:
                    Move(-1, 0, data);
                    break;
                case ConsoleKey.A:
                case ConsoleKey.LeftArrow:
                    Move(0, -1, data);
                    lookingLeft = true;
                    break;
                case ConsoleKey.D:
                case ConsoleKey.RightArrow:
                    Move(0, 1, data);
                    lookingLeft = false;
                    break;
                case ConsoleKey.Spacebar:
                    foreach (DestructableObject d in data.destructables)
                    {
                        Attack(d, true);
                    }

                    foreach (BaseEnemy e in data.enemies)
                    {
                        Attack(e, false);
                        Recoil(e, data, e.Defending);

                    }
                    break;
                case ConsoleKey.E:
                    foreach (IndestructableEntity i in data.indestructables)
                    {
                        i.Interact(this);
                    }
                    break;
                case ConsoleKey.D1:
                    if (potions[0] != null)
                    {
                        IncreaseHealth(potions[0].Healing);
                        potions[0] = null;
                    }
                    break;
                case ConsoleKey.D2:
                    if (potions[1] != null)
                    {
                        IncreaseHealth(potions[1].Healing);
                        potions[1] = null;
                    }
                    break;
                case ConsoleKey.D3:
                    if (potions[2] != null)
                    {
                        IncreaseHealth(potions[2].Healing);
                        potions[2] = null;
                    }
                    break;
                case ConsoleKey.D4:
                    if (potions[3] != null)
                    {
                        IncreaseHealth(potions[3].Healing);
                        potions[3] = null;
                    }
                    break;
                case ConsoleKey.D5:
                    if (potions[4] != null)
                    {
                        IncreaseHealth(potions[4].Healing);
                        potions[4] = null;
                    }
                    break;
                case ConsoleKey.D6:
                    if (potions[5] != null)
                    {
                        IncreaseHealth(potions[5].Healing);
                        potions[5] = null;
                    }
                    break;
                case ConsoleKey.D7:
                    if (potions[6] != null)
                    {
                        IncreaseHealth(potions[6].Healing);
                        potions[6] = null;
                    }
                    break;
                case ConsoleKey.D8:
                    if (potions[7] != null)
                    {
                        IncreaseHealth(potions[7].Healing);
                        potions[7] = null;
                    }
                    break;
                case ConsoleKey.D9:
                    if (potions[8] != null)
                    {
                        IncreaseHealth(potions[8].Healing);
                        potions[8] = null;
                    }
                    break;
            }
        }
        //to make the UML Diagram more readable
        private void SetTileList() 
        {
            charTiles.Add(rBeak);
            charTiles.Add(lBeak);
            charTiles.Add(eyes);
            charTiles.Add(rHead);
            charTiles.Add(lHead);
            charTiles.Add(rStomach);
            charTiles.Add(lStomach);
            charTiles.Add(rBack);
            charTiles.Add(lBack);
            charTiles.Add(rWeapon);
            charTiles.Add(lWeapon);
        }
    }
}