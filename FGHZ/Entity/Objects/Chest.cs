﻿using System;
using FGHZ.Engine;
using FGHZ.Entity.Units;

namespace FGHZ.Entity.Objects
{
    class Chest : IndestructableEntity
    {
        static readonly Tile unopenedChestTile = new Tile('\u2588', ConsoleColor.DarkYellow);
        static readonly Tile openedChestTile = new Tile('\u2588', ConsoleColor.Gray);
        readonly Equipment loot;
        static Random rng = new Random();
        bool opened = false;

        //chest always has size: 2x2
        public Chest(int nPosY, int nPosX) : base(nPosY, nPosX, 2, 2) 
        {
            this.loot = GenerateEquipment();
        }

        //generates random equipment
        private Equipment GenerateEquipment()
        {
            int worth = rng.Next(1,6);
            float armor;
            float dmg;
            float range;
            if (worth<= 3)
            {
                //loot is weapon
                armor = 0;
                dmg = worth;
                range = rng.Next(4, 6);
            }
            else  
            {
                //loot is armor
                armor = (worth-3);
                dmg = 0;
                range = 0;
            }
            Equipment e = new Equipment(armor, dmg, range);
            return e;
        }

        public override void Draw(Tilemap map)
        {
            if (opened)     //Draws chest differently when opened
            {
                for (int y = pos.PosY; y < (pos.PosY + hitbox.SizeY); y++)
                {
                    for (int x = pos.PosX; x < (pos.PosX + hitbox.SizeX); x++)
                    {
                        map.SetTile(y, x, openedChestTile);
                    }
                }
            }
            else
            {
                for (int y = pos.PosY; y < (pos.PosY + hitbox.SizeY); y++)
                {
                    for (int x = pos.PosX; x < (pos.PosX + hitbox.SizeX); x++)
                    {
                        map.SetTile(y, x, unopenedChestTile);
                    }
                }
            }
        }

        public override void Update(LevelData data, ConsoleKey? keyPressed) { }

        //opens chest and makes player recieve loot
        public override bool Interact(Unit unit)
        {
            if (this.GetDistance(unit) <= 7)      //chest can be opened, if distance to player is smaller/equal 7
            {
                if (opened == false)
                {
                    unit.AddEquipment(loot);
                    opened = true;
                }
            }
            return opened;
        }
    }
}
