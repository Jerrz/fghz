﻿using FGHZ.Engine;
using System;

namespace FGHZ.Entity.Units.Enemies
{
    class Enemy_normal : BaseEnemy
    {
        static readonly Tile rBeak = new Tile('>', System.ConsoleColor.Red);
        static readonly Tile lBeak = new Tile('<', System.ConsoleColor.Red);
        static readonly Tile eyes = new Tile('"', System.ConsoleColor.Red);
        static readonly Tile rHead = new Tile('(', System.ConsoleColor.Red);
        static readonly Tile lHead = new Tile(')', System.ConsoleColor.Red);
        static readonly Tile rStomach = new Tile(')', System.ConsoleColor.Red);
        static readonly Tile lStomach = new Tile('(', System.ConsoleColor.Red);
        static readonly Tile rBack = new Tile('\u002F', System.ConsoleColor.Red);
        static readonly Tile lBack = new Tile('\u005C', System.ConsoleColor.Red);
        static readonly Tile lWeapon = new Tile('\u005C', System.ConsoleColor.Red);
        static readonly Tile rWeapon = new Tile('\u002F', System.ConsoleColor.Red);

        const int Y_SIZE = 2;
        const int X_SIZE = 3;
        const int HP = 20;
        static readonly Equipment start = new Equipment(0, 3, 3);

        System.Diagnostics.Stopwatch move_watch = new System.Diagnostics.Stopwatch();
        System.Diagnostics.Stopwatch attack_watch = new System.Diagnostics.Stopwatch();
        const int WAIT_TIME_MOVE = 100, WAIT_TIME_ATTACK=500;

        public Enemy_normal(int nPosY, int nPosX) : base(nPosY, nPosX, Y_SIZE, X_SIZE, HP, start)
        {
            move_watch.Start();
            attack_watch.Start();
        }

        // Constructor for random placement
        public Enemy_normal(LevelData data) : base(Y_SIZE, X_SIZE, HP, start, data)
        {
            move_watch.Start();
            attack_watch.Start();
        }

        public override void Draw(Tilemap map)
        {
            if (lookingLeft)
            {
                map.SetTile(pos.PosY, pos.PosX, lBeak);
                map.SetTile(pos.PosY, pos.PosX + 1, eyes);
                map.SetTile(pos.PosY, pos.PosX + 2, lHead);
                map.SetTile(pos.PosY + 1, pos.PosX, lWeapon);
                map.SetTile(pos.PosY + 1, pos.PosX + 1, lStomach);
                map.SetTile(pos.PosY + 1, pos.PosX + 2, lBack);
            }
            else
            {
                map.SetTile(pos.PosY, pos.PosX, rHead);
                map.SetTile(pos.PosY, pos.PosX + 1, eyes);
                map.SetTile(pos.PosY, pos.PosX + 2, rBeak);
                map.SetTile(pos.PosY + 1, pos.PosX, rBack);
                map.SetTile(pos.PosY + 1, pos.PosX + 1, rStomach);
                map.SetTile(pos.PosY + 1, pos.PosX + 2, rWeapon);
            }

        }

        public override void Update(LevelData data, ConsoleKey? keyPressed)
        {
            if (move_watch.ElapsedMilliseconds < WAIT_TIME_MOVE)
            {
                return;
            }
            else
            {
                if (GetDistance(data.player) <= 10)
                {
                    MoveToPlayer(data);
                }
                else
                {
                    RandomWalk(data);
                }
                move_watch.Restart(); 
            }

            if (attack_watch.ElapsedMilliseconds < WAIT_TIME_ATTACK)
            {
                return;
            }
            else
            {
                Attack(data.player , false);
                attack_watch.Restart(); 
            }
        }
    }
}
