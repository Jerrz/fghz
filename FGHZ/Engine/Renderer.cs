﻿using FGHZ.Entity.EntityTools;
using FGHZ.Entity.Units.Player;
using System;
using System.Collections.Generic;

namespace FGHZ.Engine
{
    class Renderer
    {
        private readonly Tilemap map;
        private Tilemap buffer;

        private const int CONSOLE_WIDTH = 120;
        private const int CONSOLE_HEIGHT = 28;
        private const int MARGIN_WIDTH = CONSOLE_WIDTH / 2 - 1;
        private const int MARGIN_HEIGHT = CONSOLE_HEIGHT / 2 - 1;

        public Renderer(Tilemap map)
        {
            this.map = map;
            buffer = new Tilemap(CONSOLE_HEIGHT, CONSOLE_WIDTH, '!');
        }

        public void ResetBuffer()
        {
            buffer = new Tilemap(CONSOLE_HEIGHT, CONSOLE_WIDTH, '!');
        }

        private void DrawOnConsole(int nY, int nX, ConsoleColor color, char character)
        {
            Console.SetCursorPosition(nX, nY);
            Console.ForegroundColor = color;
            Console.Write(character);
        }

        // BAD RENDERER! DO NOT USE!
        // Just for testing and stuff
        public void RenderEntireMap(System.ConsoleColor color)
        {
            int mapHeight = map.GetHeight();
            int mapWidth = map.GetWidth();

            // Console.Clear();
            for(int row = 0; row < mapHeight; row++)
            {
                for(int col = 0; col < mapWidth; col++)
                {
                    Tile currentTile = map.GetTile(row, col);
                    DrawOnConsole(row, col, color, currentTile.GetCharacter());
                }
            }
        }

        public void UpdateScreenBuffered()
        {
            int mapHeight = map.GetHeight();
            int mapWidth = map.GetWidth();

            for (int row = 0; row < mapHeight; row++)
            {
                for (int col = 0; col < mapWidth; col++)
                {
                    Tile currentTile = map.GetTile(row, col);
                    if (currentTile != buffer.GetTile(row, col))
                    {
                        DrawOnConsole(row, col, currentTile.GetColor(), currentTile.GetCharacter());
                        buffer.SetTile(row, col, currentTile);
                    }
                }
            }
        }

        private void UpdateScreenBufferedArea(int yStart, int xStart, Player p)
        {
            List<Tile> invHUD = new List<Tile>(p.GetInvHUD());
            List<Tile> statsHUD = new List<Tile>(p.GetStatsHUD());
            Tile currentTile;

            for (int row = 0; row < CONSOLE_HEIGHT; row++)
            {
                for (int col = 0; col < CONSOLE_WIDTH; col++)
                {
                    if (row == CONSOLE_HEIGHT - 1) // Stats
                    {
                        if (col < statsHUD.Count)
                            currentTile = statsHUD[col];
                        else
                            currentTile = map.GetTile(row + yStart, col + xStart);
                    }
                    else if (col >= CONSOLE_WIDTH - 2) // Inv
                    {
                        if (row < invHUD.Count)
                        {
                            if (col == CONSOLE_WIDTH - 2)
                            {
                                char number = (row + 1).ToString()[0];
                                currentTile = new Tile(number, ConsoleColor.Black);
                            }
                            else
                            {
                                currentTile = invHUD[row];
                            }
                        }
                        else
                            currentTile = map.GetTile(row + yStart, col + xStart);
                    }
                    else
                    {
                        currentTile = map.GetTile(row + yStart, col + xStart);
                    }

                    if (currentTile != buffer.GetTile(row, col))
                    {
                        DrawOnConsole(row, col, currentTile.GetColor(), currentTile.GetCharacter());
                        buffer.SetTile(row, col, currentTile);
                    }
                }
            }
        }

        public void DrawMovingScreen(Player player)
        {
            Position pos = player.GetPosition();
            int yStart, xStart;

            yStart = pos.PosY - MARGIN_HEIGHT;
            xStart = pos.PosX - MARGIN_WIDTH;

            if (yStart < 0)
                yStart = 0;
            if (yStart > map.GetHeight() - CONSOLE_HEIGHT)
                yStart = map.GetHeight() - CONSOLE_HEIGHT;

            if (xStart < 0)
                xStart = 0;
            if (xStart > map.GetWidth() - CONSOLE_WIDTH)
                xStart = map.GetWidth() - CONSOLE_WIDTH;

            UpdateScreenBufferedArea(yStart, xStart, player);
        }
    }
}
