﻿using FGHZ.Engine;
using System;

namespace FGHZ.Entity.Units.Enemies
{
    abstract class BaseEnemy : Unit
    {
        protected bool lookingLeft = false;
        protected int[] moved = new int[] { 0, 0, 0, 0 }; //{right, left, up, down}   //not in use 
        protected bool[] tried = new bool[] { true, true, true, true }; // RightUp,LeftUp,RightDown,LeftDown
        public bool Defending = false;
        public BaseEnemy(int nPosY, int nPosX, int nSizeY, int nSizeX, float fHP, Equipment start) : base(nPosY, nPosX, nSizeY, nSizeX, fHP, start) { }

        // Constructor for random placement
        public BaseEnemy(int nSizeY, int nSizeX, float fHP, Equipment start, LevelData data) : base(nSizeY, nSizeX, fHP, start, data) { }

        protected bool MoveRight(LevelData data) 
        {
            if (Move(0, 1, data))
            {
                lookingLeft = false;
                return true; }
            else
                return false;
        }
        protected bool MoveLeft(LevelData data) 
        {
            if (Move(0, -1, data))
            {
                lookingLeft = true;
                return true;
            }
            else
                return false;
        }
        protected bool MoveUp(LevelData data) {
            if (Move(-1, 0, data))
                return true;
            else
                return false; 
        }
        protected bool MoveDown(LevelData data)
        {
            if (Move(1, 0, data))
                return true;
            else
                return false;
        }
        protected void MoveToPlayer(LevelData data)
        {
            int playerX = data.player.GetPosition().PosX;
            int playerY = data.player.GetPosition().PosY;

            int thisX = pos.PosX;
            int thisY = pos.PosY;

            if (thisY >= playerY)
            {
                if (thisX >= playerX)
                {
                    if (thisY - playerY >= thisX - playerX)
                    {
                        if (MoveUp(data)) 
                        {
                            moved = new int[] { 0, 0, 1, 0 };
                        }
                        else
                        if (MoveLeft(data))
                        {
                            moved = new int[] { 0, 1, 0, 0 };
                            lookingLeft = true;
                        }
                    }
                    else
                    {
                        if (MoveLeft(data)) 
                        {
                            moved = new int[] { 0, 1, 0, 0 };
                            lookingLeft = true;
                        }
                        else
                        if (MoveUp(data)) 
                        {
                            moved = new int[] { 0, 0, 1, 0 };
                        }
                    }
                }
                else
                {
                    if (thisY - playerY >= playerX - thisX)
                    {
                        if (MoveUp(data)) 
                        {
                            moved = new int[] { 0, 0, 1, 0 };
                        }
                        else
                        if (MoveRight(data)) 
                        {
                            moved = new int[] { 1, 0, 0, 0 };
                            lookingLeft = false;
                        }
                    }
                    else
                    {
                        if (MoveRight(data)) 
                        {
                            moved = new int[] { 1, 0, 0, 0 };
                            lookingLeft = false;
                        }
                        else
                        if (MoveUp(data)) 
                        {
                            moved = new int[] { 0, 0, 1, 0 };
                        }
                    }
                }
            }
            else
            {
                if (thisX >= playerX)
                {
                    if (playerY - thisY >= thisX - playerX)
                    {
                        if (MoveDown(data)) 
                        {
                            moved = new int[] { 0, 0, 0, 1 };
                        }
                        else
                        if (MoveLeft(data)) 
                        {
                            moved = new int[] { 0, 1, 0, 0 };
                            lookingLeft = true;
                        }
                    }
                    else
                    {
                        if (MoveLeft(data)) 
                        {
                            moved = new int[] { 0, 1, 0, 0 };
                            lookingLeft = true;
                        }
                        else
                        if (MoveDown(data)) 
                        {
                            moved = new int[] { 0, 0, 0, 1 };
                        }
                    }
                }
                else
                {
                    if (playerY - thisY >= playerX - thisX)
                    {
                        if (MoveDown(data)) 
                        {
                            moved = new int[] { 0, 0, 0, 1 };
                        }
                        else
                        if (MoveRight(data)) 
                        {
                            moved = new int[] { 1, 0, 0, 0 };
                            lookingLeft = false;
                        }
                    }
                    else
                    {
                        if (MoveRight(data)) 
                        {
                            moved = new int[] { 1, 0, 0, 0 };
                            lookingLeft = false;
                        }
                        else
                        if (MoveDown(data)) 
                        {
                            moved = new int[] { 0, 0, 0, 1 };
                        }
                    }
                }
            }
        }

        protected void RandomWalk(LevelData data) 
        {
            Random rnd = new Random();
            int toWhere = rnd.Next(1, 4);
            switch(toWhere)
            {
                case 1:
                    MoveRight(data);
                    break;
                case 2:
                    MoveLeft(data);
                    break;
                case 3:
                    MoveUp(data);
                    break;
                default:
                    MoveDown(data);
                    break;
            }
        }

        protected bool Tried()
        {
            for (int i = 0; i < 4; i++)
            {
                if (!tried[i])
                    return false;
            }
            return true;
        }
    }
}
