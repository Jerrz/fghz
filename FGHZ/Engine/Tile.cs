﻿namespace FGHZ.Engine
{
    class Tile
    {
        private System.ConsoleColor color;
        private char character;

        public Tile(char cStdChar = '\u2588')
        {
            color = System.ConsoleColor.White;
            character = cStdChar;
        }

        public Tile(char character, System.ConsoleColor color)
        {
            this.character = character;
            this.color = color;
        }

        public Tile(Tile t)
        {
            color = t.color;
            character = t.character;
        }

        public void SetTile(System.ConsoleColor color, char character)
        {
            this.color = color;
            this.character = character;
        }

        public System.ConsoleColor GetColor()
        {
            return color;
        }

        public char GetCharacter()
        {
            return character;
        }

        public static bool operator ==(Tile a, Tile b)
        {
            if (a.character != b.character)
                return false;
            if (a.color != b.color)
                return false;
            return true;
        }

        public static bool operator !=(Tile a, Tile b)
        {
            return !(a == b);
        }
    }
}
