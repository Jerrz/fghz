﻿namespace FGHZ.Entity.EntityTools
{
    class Position
    {
        private int nY, nX;

        public int PosX { get => nX; set => nX = value; }
        public int PosY { get => nY; set => nY = value; }

        public Position(int nY, int nX)
        {
            this.nY = nY;
            this.nX = nX;
        }

        public Position(Position pos)
        {
            nY = pos.nY;
            nX = pos.nX;
        }
    }
}