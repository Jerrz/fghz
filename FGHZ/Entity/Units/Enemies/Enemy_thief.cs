﻿using FGHZ.Engine;
using FGHZ.Entity.Objects;
using FGHZ.Entity.EntityTools;
using System;
using System.Collections.Generic;

namespace FGHZ.Entity.Units.Enemies
{
    class Enemy_thief : BaseEnemy
    {
       // _(">
       //\___)
        static readonly Tile rBeak = new Tile('>', System.ConsoleColor.Green);
        static readonly Tile lBeak = new Tile('<', System.ConsoleColor.Green);
        static readonly Tile eyes = new Tile('"', System.ConsoleColor.Green);
        static readonly Tile rHead = new Tile('(', System.ConsoleColor.Green);
        static readonly Tile lHead = new Tile(')', System.ConsoleColor.Green);
        static readonly Tile rStomach = new Tile(')', System.ConsoleColor.Green);
        static readonly Tile lStomach = new Tile('(', System.ConsoleColor.Green);
        static readonly Tile body = new Tile('_', System.ConsoleColor.Green); 
        static readonly Tile rBack = new Tile('\u002F', System.ConsoleColor.Green);
        static readonly Tile lBack = new Tile('\u005C', System.ConsoleColor.Green);
        
        const int Y_SIZE = 2;
        const int X_SIZE = 5;
        const int HP = 15;
        static readonly Equipment start = new Equipment(5, 0, 5);
        int counter=0;
        List<Position> ChestPosition = new List<Position>();
        System.Diagnostics.Stopwatch spacing_watch = new System.Diagnostics.Stopwatch();
        System.Diagnostics.Stopwatch move_watch = new System.Diagnostics.Stopwatch();
        System.Diagnostics.Stopwatch attack_watch = new System.Diagnostics.Stopwatch();
        const int WAIT_TIME_MOVE_FAST = 25, WAIT_TIME_MOVE_SLOW = 500, WAIT_TIME_OPEN = 1500;

        public Enemy_thief(int nPosY, int nPosX) : base(nPosY, nPosX, Y_SIZE, X_SIZE, HP, start)
        {
            spacing_watch.Start();
            move_watch.Start();
            attack_watch.Start();
        }        
        
        // Constructor for random placement
        public Enemy_thief(LevelData data) : base(Y_SIZE, X_SIZE, HP, start, data)
        {
            spacing_watch.Start();
            move_watch.Start();
            attack_watch.Start();
        }

        public override void Draw(Tilemap map)
        {
            if (lookingLeft)
            {
                map.SetTile(pos.PosY, pos.PosX, lBeak);
                map.SetTile(pos.PosY, pos.PosX + 1, eyes);
                map.SetTile(pos.PosY, pos.PosX + 2, lHead); 
                map.SetTile(pos.PosY, pos.PosX + 3, body); 
                map.SetTile(pos.PosY, pos.PosX + 4,body);
                map.SetTile(pos.PosY+1, pos.PosX, lStomach);
                map.SetTile(pos.PosY+1, pos.PosX + 1, body);
                map.SetTile(pos.PosY+1, pos.PosX + 2, body);
                map.SetTile(pos.PosY+1, pos.PosX + 3, body);
                map.SetTile(pos.PosY+1, pos.PosX + 4, rBack);
            }
            else
            {
                map.SetTile(pos.PosY, pos.PosX, body);
                map.SetTile(pos.PosY, pos.PosX + 1, body);
                map.SetTile(pos.PosY, pos.PosX + 2, rHead);
                map.SetTile(pos.PosY, pos.PosX + 3, eyes);
                map.SetTile(pos.PosY, pos.PosX + 4, rBeak);
                map.SetTile(pos.PosY + 1, pos.PosX, lBack);
                map.SetTile(pos.PosY + 1, pos.PosX + 1, body);
                map.SetTile(pos.PosY + 1, pos.PosX + 2, body);
                map.SetTile(pos.PosY + 1, pos.PosX + 3, body);
                map.SetTile(pos.PosY + 1, pos.PosX + 4, rStomach);
            }
        }

        public override void Update(LevelData data, ConsoleKey? keyPressed) 
        {

            if (ChestPosition.Count == 0)
            {
                foreach (IndestructableEntity i in data.indestructables)
                {
                    if (i is Chest)
                    {
                        bool k = false;
                        foreach(Position j in ChestPosition) 
                        { 
                            if(j==i.GetPosition())
                            {
                                k = true;
                            }
                        }
                        if (!k)
                        {
                            ChestPosition.Add(i.GetPosition());
                        }
                    }
                }
            } 
            if (move_watch.ElapsedMilliseconds < WAIT_TIME_MOVE_FAST)
            {
                return;
            } 
            else
            {
                if (Tried()) 
                {
                    if (GetDistance(data.player) <= 20)
                    {
                        if (move_watch.ElapsedMilliseconds < WAIT_TIME_MOVE_SLOW && spacing_watch.ElapsedMilliseconds >= 100)
                        {
                            spacing_watch.Restart();
                            MoveAwayFromPlayer(data);
                        }
                        else
                        {
                            if (move_watch.ElapsedMilliseconds < (2 * (WAIT_TIME_MOVE_SLOW)) && spacing_watch.ElapsedMilliseconds >= 200)
                            {
                                spacing_watch.Restart();
                                MoveAwayFromPlayer(data);
                            }
                            if (move_watch.ElapsedMilliseconds > (2 * (WAIT_TIME_MOVE_SLOW)))
                            {
                                spacing_watch.Restart();
                                move_watch.Restart();
                            }
                        }
                    } 
                    else
                    {
                        if (move_watch.ElapsedMilliseconds >= 150)
                        {
                            if (ChestPosition.Count != 0) 
                            {
                                MoveToChest(data);
                            }
                            else                         
                            {
                                RandomWalk(data);
                            }
                            spacing_watch.Restart();
                            move_watch.Restart();
                        }
                    }

                }

                else 
                {
                    FindWay(data);
                }
            }
            if (attack_watch.ElapsedMilliseconds < WAIT_TIME_OPEN)      
            {
                return;
            }
            else
            {
                foreach (IndestructableEntity i in data.indestructables)
                {
                    if (!(i is Chest))
                    {
                        continue;
                    }
                    else
                    {
                        Chest c = (Chest)i;
                        if (c.Interact(this))
                        {
                            for (int j = ChestPosition.Count - 1; j >= 0; j--)
                            {
                                Position k = ChestPosition[j];
                                if (k == c.GetPosition())
                                    ChestPosition.Remove(k);
                            }

                        }
                    }
                }
                attack_watch.Restart();
            }
        }

        protected void MoveAwayFromPlayer(LevelData data)
        {
            int playerX = data.player.GetPosition().PosX;
            int playerY = data.player.GetPosition().PosY;

            int thisX = pos.PosX;
            int thisY = pos.PosY;

            if (thisY >= playerY)
            {
                if (thisX >= playerX) // player is left up of the enemy
                {
                    if (thisY - playerY >= thisX - playerY)//check the distance difference X-axe Y-axe
                    {
                        if (MoveDown(data))
                        {
                            moved = new int[] { 0, 0, 0, 1 }; 
                        }
                        else
                        {
                            if (MoveRight(data))
                            {
                                moved = new int[] { 1, 0, 0, 0 };
                            }
                            else
                            {
                                tried[2] = false;
                            }
                        }
                    }
                    else
                    {
                        if (MoveRight(data))
                        {
                            moved = new int[] { 1, 0, 0, 0 };
                        }
                        else
                        {
                            if (MoveDown(data))
                            {
                                moved = new int[] { 0, 0, 0, 1 };
                            }
                            else
                            {
                                tried[2] = false;
                            }
                        }
                    }
                }
                else //player is right up the enemy
                {
                    if (thisY - playerY >= playerX - thisX) 
                    {
                        if (MoveDown(data))
                        {
                            moved = new int[] { 0, 0, 0, 1 };
                        }
                        else
                        {
                            if (MoveLeft(data))
                            {
                                moved = new int[] { 0, 1, 0, 0 };
                            }
                            else
                            {
                                tried[3] = false;
                            }
                        }
                    }
                    else
                    {
                        if (MoveLeft(data))
                        {
                            moved = new int[] { 0, 1, 0, 0 };
                        }
                        else
                        {
                            if (MoveDown(data))
                            {
                                moved = new int[] { 0, 0, 0, 1 };
                            }
                            else
                            {
                                tried[3] = false;
                            }
                        }
                    }
                }
            }
            else
            {
                if (thisX >= playerX) //player is left down of the enemy
                {
                    if (playerY - thisY >= thisX - playerX)
                    {

                        if (MoveUp(data))
                        {
                            moved = new int[] { 0, 0, 1, 0 };
                        }
                        else
                        {
                            if (MoveRight(data))
                            {
                                moved = new int[] { 1, 0, 0, 0 };
                            }
                            else
                            {
                                tried[0] = false;
                            }
                        }
                    }
                    else
                    {
                        if (MoveRight(data))
                        {
                            moved = new int[] { 1, 0, 0, 0 };
                        }
                        else
                        {
                            if (MoveUp(data))
                            {
                                moved = new int[] { 0, 0, 1, 0 };
                            }
                            else
                            {
                                tried[0] = false;
                            }
                        }

                    }
                }
                else //player is right down of the enemy
                {
                    if (playerY - thisY >= playerX - thisX)
                    {
                        if (MoveUp(data))
                        {
                            moved = new int[] { 0, 0, 1, 0 };
                        }
                        else
                        {
                            if (MoveLeft(data))
                            {
                                moved = new int[] { 0, 1, 0, 0 };
                            }
                            else
                            {
                                tried[1] = false;
                            }
                        }
                    }
                    else
                    {
                        if (MoveLeft(data))
                        {
                            moved = new int[] { 0, 1, 0, 0 };
                        }
                        else
                        {
                            if (MoveUp(data))
                            {
                                moved = new int[] { 0, 0, 1, 0 };
                            }
                            else
                            {
                                tried[1] = false;
                            }
                        }
                    }
                }
            }
        }

        protected void MoveToChest(LevelData data)
        {
            int chestX = ShortestDistance(data).PosX;
            int chestY = ShortestDistance(data).PosY;

            int thisX = pos.PosX;
            int thisY = pos.PosY;

            if (thisY == chestY && !(thisX==chestX))
            {
                if (thisX < chestX && !MoveRight(data))
                {
                    tried[0] = false;
                }
                else
                {
                    if (!MoveLeft(data) )
                    {
                        tried[3] = false;
                    }
                }
            }
            else 
            {
                if(thisX==chestX && !(thisY ==chestY))
                {
                    if (thisY < chestY && !MoveDown(data))
                    {
                        tried[2] = false;
                    }
                    else
                    {
                        if (!MoveUp(data))
                        {
                            tried[1] = false;
                        }
                    }
                }
            }

            if (thisY >= chestY)
            {
                if (thisX >= chestX)
                {
                    if (thisY - chestY >= thisX - chestY)
                    {
                        if (MoveUp(data))
                        {
                            moved = new int[] { 0, 0, 1, 0 };
                        }
                        else
                        {
                            if (MoveLeft(data))
                            {
                                moved = new int[] { 0, 1, 0, 0 };
                            }
                            else
                            {
                                tried[1] = false;
                            }
                        }
                    }
                    else
                    {
                        if (MoveLeft(data))
                        {
                            moved = new int[] { 0, 1, 0, 0 };
                        }
                        else
                        {
                            if (MoveUp(data))
                            {
                                moved = new int[] { 0, 0, 1, 0 };
                            }
                            else
                            {
                                tried[1] = false;
                            }
                        }
                    }
                }
                else
                {
                    if (thisY - chestY >= chestX - thisX)
                    {
                        if (MoveUp(data))
                        {
                            moved = new int[] { 0, 0, 1, 0 };
                        }
                        else
                        {
                            if (MoveRight(data))
                            {
                                moved = new int[] { 1, 0, 0, 0 };
                            }
                            else
                            {
                                tried[0] = false;
                            }
                        }
                    }
                    else
                    {
                        if (MoveRight(data))
                        {
                            moved = new int[] { 1, 0, 0, 0 };
                        }
                        else
                        {
                            if (MoveUp(data))
                            {
                                moved = new int[] { 0, 0, 1, 0 };
                            }
                            else
                            {
                                tried[0] = false;
                            }
                        }
                    }
                }
            }
            else
            {
                if (thisX >= chestX)
                {
                    if (chestY - thisY >= thisX - chestX)
                    {
                        if (MoveDown(data))
                        {
                            moved = new int[] { 0, 0, 0, 1 };
                        }
                        else
                        {
                            if (MoveLeft(data))
                            {
                                moved = new int[] { 0, 1, 0, 0 };
                            }
                            else
                            {
                                tried[3] = false;
                            }
                        }
                    }
                    else
                    {
                        if (MoveLeft(data))
                        {
                            moved = new int[] { 0, 1, 0, 0 };
                        }
                        else
                        {
                            if (MoveDown(data))
                            {
                                moved = new int[] { 0, 0, 0, 1 };
                            }
                            else
                            {
                                tried[3] = false;
                            }
                        }
                    }
                }
                else
                {
                    if (chestY - thisY >= chestX - thisX)
                    {
                        if (MoveDown(data))
                        {
                            moved = new int[] { 0, 0, 0, 1 };
                        }
                        else
                        {
                            if (MoveRight(data))
                            {
                                moved = new int[] { 1, 0, 0, 0 };
                            }
                            else
                            {
                                tried[2] = false;
                            }
                        }
                    }
                    else
                    {
                        if (MoveRight(data))
                        {
                            moved = new int[] { 1, 0, 0, 0 };
                        }
                        else
                        {
                            if (MoveDown(data))
                            {
                                moved = new int[] { 0, 0, 0, 1 };
                            }
                            else
                            {
                                tried[2] = false;
                            }
                        }
                    }
                }
            }
        }

        protected void FindWay(LevelData data) 
        {
            

            if (!tried[0])//UpRight
            {
                if (counter >= 2 && MoveUp(data))
                {
                    spacing_watch.Restart();
                    counter = 0;

                    tried[0] = true;
                }
                else{

                    if (MoveRight(data))
                    {
                        spacing_watch.Restart();
                        counter++;

                    }
                    else
                    {
                        if (MoveDown(data))
                        {
                            spacing_watch.Restart();
                        }
                        else
                        {
                            tried[0] = true;
                            tried[2] = false;
                        }
                    }
                }

            }
            else 
            {
                if (!tried[1])//UpLeft
                {
                    if (counter >= 2 && MoveLeft(data))
                    {
                        spacing_watch.Restart();
                        counter = 0;
                        tried[1] = true;

                    }
                    else
                    {
                        if (MoveUp(data))
                        {
                            spacing_watch.Restart();
                            counter++;
                        }
                        else
                        {
                            if (MoveRight(data))
                            {
                                spacing_watch.Restart();
                            }
                            else
                            {
                                tried[1] = true;
                                tried[0] = false;
                            }
                        }
                    }
                }
                else 
                {
                    if (!tried[2])//DownRight
                    {
                        if (counter >= 2 && MoveRight(data))
                        {
                            spacing_watch.Restart();
                            counter = 0;
                            tried[2] = true;
                        }
                        else
                        {
                            if (MoveDown(data))
                            {
                                spacing_watch.Restart();
                                counter++;
                            }
                            else
                            {
                                if (MoveLeft(data))
                                {
                                    spacing_watch.Restart();
                                }
                                else
                                {
                                    tried[2] = true;
                                    tried[3] = false;
                                }
                            }
                        }
                    }
                    else //DownLeft
                    {
                        if (counter >= 2 && MoveDown(data))
                        {
                            spacing_watch.Restart();
                            counter = 0;
                            tried[3] = true;
                        }
                        else
                        {
                            if (MoveLeft(data))
                            {
                                spacing_watch.Restart();
                                counter++;

                            }
                            else
                            {
                                if (MoveUp(data))
                                {
                                    spacing_watch.Restart();
                                }
                                else
                                {
                                    tried[3] = true;
                                    tried[1] = false;
                                }
                            }
                        }
                    }
                }
            }
        }

        protected Position ShortestDistance(LevelData data) 
        {
            Position fictivPosition = ChestPosition[0];
            foreach (Position i in ChestPosition)
            {
                if (Math.Sqrt(fictivPosition.PosX * fictivPosition.PosX + fictivPosition.PosY * fictivPosition.PosY) >= Math.Sqrt(i.PosX * i.PosX + i.PosY * i.PosY))
                {
                    fictivPosition = i;
                }
                continue;
            }
            return fictivPosition;
        }
    }
}
