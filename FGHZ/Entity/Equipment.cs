﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FGHZ.Entity
{
    struct Equipment
    {
        public float fArmor, fDmg, fRange;

        public Equipment(float armor, float dmg, float range)
        {
            fArmor = armor;
            fDmg = dmg;
            fRange = range;
        }
    }
}