﻿namespace FGHZ.Engine
{
    class Tilemap
    {
        private Tile[,] tiles;
        private readonly int nHeight, nWidth;

        public Tilemap(int nHeight, int nWidth, char cStdChar = '\u2588')
        {
            this.nHeight = nHeight;
            this.nWidth = nWidth;

            tiles = new Tile[nHeight, nWidth];
            for(int i = 0; i < nHeight; i++)
            {
                for(int j = 0; j < nWidth; j++)
                {
                    tiles[i, j] = new Tile(cStdChar);
                }
            }
        }

        public void Reset(Tilemap m)
        {
            for (int i = 0; i < nHeight; i++)
            {
                for (int j = 0; j < nWidth; j++)
                {
                    if (tiles[i, j] != m.tiles[i, j])
                        tiles[i, j] = new Tile(m.tiles[i, j]);
                }
            }
        }

        public void SetTile(int nY, int nX, System.ConsoleColor color, char character)
        {
            tiles[nY, nX].SetTile(color, character);
        }

        public void SetTile(int nY, int nX, Tile t)
        {
            tiles[nY, nX] = new Tile(t);
        }

        public Tile GetTile(int nY, int nX)
        {
            return tiles[nY, nX];
        }

        public int GetHeight()
        {
            return nHeight;
        }

        public int GetWidth()
        {
            return nWidth;
        }
    }
}
