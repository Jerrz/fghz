﻿using System;
using FGHZ.Engine;
using FGHZ.Entity.Units;

namespace FGHZ.Entity.Objects
{
    class Wall : IndestructableEntity
    {
        static readonly Tile walltile = new Tile('#', ConsoleColor.DarkRed);

        public Wall(int nPosY, int nPosX, int nSizeY, int nSizeX) : base(nPosY, nPosX, nSizeY, nSizeX) { }

        public override void Draw(Tilemap map)
        {
            for (int y = pos.PosY; y < (pos.PosY + hitbox.SizeY); y++)
            {
                for (int x = pos.PosX; x < (pos.PosX + hitbox.SizeX); x++)
                {
                    map.SetTile(y, x, walltile);
                }
            }
        }

        public override void Update(LevelData data, ConsoleKey? keyPressed) { }

        public override bool Interact(Unit unit) { return true; }
    }
}
